package com.example.springdemo.services;

import com.example.springdemo.dto.IntakeDTO;
import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.builders.IntakeBuilder;
import com.example.springdemo.entities.Intake;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class IntakeService {

    private final PatientRepository patientRepository;
    private final MedicationRepository medicationRepository;
    private final IntakeRepository intakeRepository;

    @Autowired
    public IntakeService(PatientRepository patientRepository, MedicationRepository medicationRepository, IntakeRepository intakeRepository) {
        this.patientRepository = patientRepository;
        this.medicationRepository = medicationRepository;
        this.intakeRepository = intakeRepository;
    }

    public IntakeDTO findById(Integer id){
        Optional<Intake> intake  = intakeRepository.findById(id);
        return IntakeBuilder.generateDTOFromEntity(intake.get());
    }

    public Integer insert(IntakeDTO intakeDTO) {
        Optional<Patient> patient = patientRepository.findById(intakeDTO.getPatientId());
        Optional<Medication> medication = medicationRepository.findById(intakeDTO.getMedicationId());
        Intake intake = new Intake(intakeDTO.getStartDate(), intakeDTO.getEndDate(), intakeDTO.isTaken());

        intake.setMedication(medication.get());
        intake.setPatient(patient.get());
        return intakeRepository
                .save(intake)
                .getId();
    }
    public List<IntakeDTO> findAllIntakes() {
        List<Intake> intakes = intakeRepository.getAllOrdered();
        List<Intake> notTakenIntakes = new ArrayList<>();
        for (Intake intake: intakes) {
            if(intake.isTaken() != 1) {
                notTakenIntakes.add(intake);
            }
        }

        return notTakenIntakes.stream()
                .map(IntakeBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }
    public void delete(Integer id){
        this.intakeRepository.deleteById(id);
    }

    public Integer update(IntakeDTO caregiverDTO) {
        Intake caregiver1 = IntakeBuilder.generateEntityFromDTO(caregiverDTO);
        caregiver1.setId(caregiverDTO.getId());
        caregiver1.setTaken(1);
        caregiver1.setPatient(patientRepository.findById(1).get());
        caregiver1.setMedication(medicationRepository.findById(caregiverDTO.getMedicationId()).get());
        return intakeRepository.save(caregiver1).getId();
    }

    public List<String> findMedications() {
        List<Medication> medications = medicationRepository.findAll();
        List<String> names = new ArrayList<>();
        for (Medication medication: medications
             ) {
            names.add(medication.getName());
        }
        return names;
    }
}