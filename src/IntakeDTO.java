package com.example.springdemo.dto;

import com.example.springdemo.entities.Caregiver;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

public class IntakeDTO implements Serializable {

    private Integer id;
    private Timestamp startDate;
    private Timestamp endDate;
    private Integer medicationId;
    private Integer patientId;
    private Integer taken;

    public IntakeDTO() {
    }

    public IntakeDTO(Integer id, Timestamp startDate, Timestamp endDate, Integer medicationId) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.medicationId = medicationId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Integer getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(Integer medicationId) {
        this.medicationId = medicationId;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer isTaken() {
        return taken;
    }

    public void setTaken(Integer taken) {
        this.taken = taken;
    }
}
