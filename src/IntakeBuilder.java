package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.IntakeDTO;
import com.example.springdemo.entities.Intake;

public class IntakeBuilder {

  private IntakeBuilder() {
  }

  public static IntakeDTO generateDTOFromEntity(Intake caregiver) {
    return new IntakeDTO(
            caregiver.getId(),
            caregiver.getStartDate(),
            caregiver.getEndDate(),
            caregiver.getMedication().getId());
  }
  public static Intake generateEntityFromDTO(IntakeDTO caregiverDTO){
    return new Intake(
            caregiverDTO.getStartDate(),
            caregiverDTO.getEndDate(),
            caregiverDTO.isTaken());
  }
}