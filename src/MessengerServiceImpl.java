package com.example.springdemo.services;

import com.example.springdemo.dto.IntakeDTO;
import com.example.springdemo.entities.Intake;
import org.springframework.stereotype.Component;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

@Component
public class MessengerServiceImpl implements MessengerService {

    private final IntakeService intakeService;

    public MessengerServiceImpl(IntakeService intakeService) {
        this.intakeService = intakeService;
        try {
            createStubAndBind();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public String sendMessage(String clientMessage) {
        System.out.println(clientMessage);
        return clientMessage;
    }

    public void createStubAndBind() throws RemoteException {

        MessengerService stub = (MessengerService) UnicastRemoteObject.exportObject((MessengerService) this, 0);
        Registry registry = LocateRegistry.createRegistry(1099);
        registry.rebind("MessengerService", stub);
    }
    public List<IntakeDTO> sendList(String clientMessage) throws RemoteException {
        List<IntakeDTO> intakes = new ArrayList<>();
        if (clientMessage.equals("Client Message")) {
            intakes = intakeService.findAllIntakes();
        }
        return intakes;
    }
    public String deleteRow(String clientMessage) {
        intakeService.update(intakeService.findById(Integer.parseInt(clientMessage)));
        return "ok";
    }

    public List<String> medicationList() throws RemoteException {
        return intakeService.findMedications();
    }
}
