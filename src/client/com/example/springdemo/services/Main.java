package com.example.springdemo.services;

import com.example.springdemo.dto.IntakeDTO;

import javax.swing.*;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main
{
    public static void main(String arg[]) throws Exception
    {
        System.setProperty("java.rmi.server.hostname","192.168.56.1");
        Registry registry = LocateRegistry.getRegistry();
        MessengerService server = (MessengerService) registry
                .lookup("MessengerService");
        String responseMessage = server.sendMessage("Client Message");
        List<IntakeDTO> intakeDTOS = server.sendList("Client Message");
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    server.sendMessage("Client Message");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        };
        System.out.println(responseMessage);
        JFrame frame = new JFrame("Dispenser");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new Dispenser(intakeDTOS, server));
        frame.setLocationByPlatform( true );
        frame.pack();
        frame.setVisible( true );
    }
}
