package com.example.springdemo.services;

import com.example.springdemo.dto.IntakeDTO;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class Dispenser extends JPanel{
    private JTable table1;
    private JLabel label;
    private JTextField textField;
    private JButton takenButton;
    JScrollPane pane;
    DefaultTableModel model = new DefaultTableModel();
    Object[] columns = {"id", "start", "end", "medication"};
    boolean ok = false;

    public Dispenser(List<IntakeDTO> intakeDTOS, MessengerService server) throws RemoteException {
        label = new JLabel( new Date().toString() );
        add(label);
        table1 = new JTable();
        pane = new JScrollPane(table1);
        add(pane);
        pane.setBounds(300, 10, 250, 500);
        System.out.println(intakeDTOS.get(0).getStartDate());
        updateTable(server.sendList("Client Message"), server.medicationList());

        add(textField);
        add(takenButton);
        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date date = new Date();
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.setTime(date);
                label.setText( date.toString() );
                if (calendar.get(Calendar.HOUR_OF_DAY) == 23 && calendar.get(Calendar.MINUTE) == 6 && calendar.get(Calendar.SECOND) == 0) {
                    try {
                        updateTable(server.sendList("Client Message"), server.medicationList());
                    } catch (RemoteException ex) {
                        ex.printStackTrace();
                    }
                }
                for (IntakeDTO intakeDTO : intakeDTOS) {
                    Timestamp t = intakeDTO.getEndDate();
                    if(t.before(date) && ok == false){
                        try {
                            server.sendMessage("notTaken" + intakeDTO.getId());
                        } catch (RemoteException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                ok = true;
            }
        });

        timer.setInitialDelay(1);
        timer.start();
        table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        takenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(table1.getSelectedRow() != -1) {
                    // remove selected row from the model
                    try {
                        server.deleteRow(String.valueOf(table1.getValueAt(table1.getSelectedRow(), 0)));
                    } catch (RemoteException ex) {
                        ex.printStackTrace();
                    }
                    System.out.println("ccc" + table1.getValueAt(table1.getSelectedRow(), 0));
                    model.removeRow(table1.getSelectedRow());
                    JOptionPane.showMessageDialog(null, "Selected row deleted successfully");
                }
            }
        });
    }

    public void updateTable(List<IntakeDTO> intakeDTOS, List<String> list) {
        String[][] objects = new String[intakeDTOS.size()][];
        int i = 0;
        for (IntakeDTO intakeDTO : intakeDTOS) {
            objects[i] = new String[4];
            objects[i][0] = intakeDTO.getId().toString();
            objects[i][1] = intakeDTO.getStartDate().toString();
            objects[i][2] = intakeDTO.getEndDate().toString();
            objects[i][3] = list.get(intakeDTO.getMedicationId() - 1);
            i++;
        }
        model.setDataVector(objects, columns);
        table1.setModel(model);
    }
}
