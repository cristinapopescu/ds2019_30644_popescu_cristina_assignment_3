package com.example.springdemo.services;

import com.example.springdemo.dto.IntakeDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface MessengerService extends Remote {
    public String sendMessage(String clientMessage) throws RemoteException;
    List<IntakeDTO> sendList(String clientMessage) throws RemoteException;
    String deleteRow(String clientMessage) throws RemoteException;
    List<String> medicationList() throws RemoteException;
}